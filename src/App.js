import React from 'react';
import {Route, Redirect, BrowserRouter, Switch} from 'react-router-dom';
// components
import SiteNavBar from './components/SiteNavBar/SiteNavBar';
// import SiteHeader from './components/SiteHeader/SiteHeader';
import SiteFooter from './components/SiteFooter/SiteFooter';
import Products from './components/Products/Products';
// pages
import Customers from './components/Customers/Customers';
import Resources from './components/pages/Resources';
import About from './components/pages/About';
import Contact from './components/pages/Contact';
import Clients from './components/Clients/Clients';
import ClientDetails from './components/Clients/ClientDetails';
import Home from './components/pages/Home';
import NotFound from './components/pages/NotFound';


const AppLayout = (props) => (
  <div>
    {props.children}
  </div>
);

const App = () => (
  <BrowserRouter>
    <AppLayout>
      <SiteNavBar />
      {/*  <SiteHeader />  */}
      <Switch>
        <Route path="/customers" component={Customers}/>
        <Route path="/resources" component={Resources}/>
        <Route path="/about"     component={About}/>
        <Route path="/contact"   component={Contact}/>
        <Route path="/products"  component={Products}/>
        <Route path="/clients"   component={Clients}/>
        <Route path="/client"    component={ClientDetails}/>
        <Route path="/client/:id"  component={ClientDetails}/>
        <Route path="/home"      component={Home}/>
        <Route path="/" exact    component={Home} />
        <Redirect exact from="/" to="/home"/>
        <Route component={NotFound} />
      </Switch>
      <SiteFooter />
    </AppLayout>
  </BrowserRouter>
);

export default App;
