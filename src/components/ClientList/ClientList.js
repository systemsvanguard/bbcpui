// src/components/ClientList/ClientList.js
// import React from 'react'
import React from 'react';
import axios from 'axios';
import { NavLink } from 'react-router-dom';

export default class ClientList extends React.Component {
  state = {
    clients: []
  }

  componentDidMount() {
    axios.get(`https://bbcpapi.openode.io/apidb` ,{ params: { _limit: 7 }}  )
      .then(res => {
        const clients = res.data;
        this.setState({ clients });
      })
  }

  render() {
    return (
      <div style={{overFlowX: "auto"}}>
        <div className="myheadings2">Recently Searched <span className="iconHeader"><NavLink to="/about" ><i className="fas fa-sync" style={{color: "#ec111a", fontSize: "20px" }} ></i></NavLink>   </span> </div>

	      <table className="bbcptable">
          <thead><tr><th>Client Name</th><th>Type</th><th>Mailing Street</th><th>City</th></tr></thead>
          <tbody>
              {
            this.state.clients.map(
              client => <tr>
              <td> <NavLink to='/'> {client.clientname} </NavLink> </td>
              <td>{client.type}</td>
              <td>{client.mailing_street}</td>
              <td>{client.mailing_city}</td>
            </tr>
            )
          }
          </tbody>
       </table>
       <br /><br />
	   </div>
    )
  }
}
