class Invoices extends Component {
  constructor(props) {
    super(props);
    this.state = {
      invoices: []
    }
  }

  componentDidMount() {
    const today = new Date();
    const yesterday = new Date();
    yesterday.setDate(today.getDate() - 1);
    const tomorrow = new Date();
    yesterday.setDate(today.getDate() + 1);

    const data = [
      {
        id: 1,
        to: 'Mr. Smith',
        amount: 10,
        paid: true,
        due: today,
      },
      {
        id: 2,
        to: 'Mrs. Jones',
        amount: 100,
        paid: false,
        due: tomorrow
      },
      {
        id: 3,
        to: 'Mrs. Black',
        amount: 50,
        paid: false,
        due: tomorrow
      },
    ];

    this.setState({invoices: data})
  }

  render() {
    const {invoices} = this.state;

    return (
      <Layout>
        <Media query={{maxWidth: 599}}>
          {screenIsSmall => screenIsSmall ?
            <InvoicesSmallScreen invoices={invoices}/>
            : <InvoicesBigScreen invoices={invoices}/>
          }
        </Media>
      </Layout>
    )
  }
}

export default Invoices;