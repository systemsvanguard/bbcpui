// src/components/ClientList/InvoiceList2.js
// import React from 'react'
import React from 'react';
// import axios from 'axios';
import { NavLink, Link } from 'react-router-dom';

const InvoiceList2 = (props) => {
  return <ul>
    <li key="dashboard">
      <Link to="/invoices/dashboard"><span>Dashboard</span></Link>
    </li>

    {props.invoices.map((invoice) =>
      <li key={invoice.id}>
        <p><Link to={`/invoices/${invoice.id}`}><span>To:</span>{invoice.to}</Link></p>
      </li>
    )}
  </ul>
};
