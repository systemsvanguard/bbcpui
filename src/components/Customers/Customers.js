import React, {Component } from 'react';
import {Route, Switch, NavLink} from 'react-router-dom';

const CustomerList = (props) => {
  return (
    <div className="container">
      <h1 className="fontBold">Customer List</h1><br />
	  {/* Display Customer List here */}
      <ul>
          {props.customers.map((customer) =>
            <li key={customer.id}>
              <p><span>Customer : </span>
			    <NavLink to={`/customers/${customer.id}`}>{customer.clientname}</NavLink>
			  </p>
            </li>
          )}
      </ul>
    </div>
  )
};

const Customer = (props) => {
  // Display Customers Details here
  const id = parseInt(props.match.params.id);
  const customer = props.customers.find(customer => customer.id === id);

  return <div className="container has-text-link is-size-4">
    <br /><br />
    Customer #{customer.id}, to customer <strong>{customer.clientname}</strong> at {customer.mailing_street}, {customer.mailing_city} ON Canada | Size {customer.size} .
  </div>
};


const Layout = (props) => (
  <div> {props.children}  </div>
);


const CustomersDisplay = (props) => {
  const {customers} = props;
  return (<div>
    <CustomerList customers={customers}/>
    <Switch>
      <Route path="/customers/:id" render={props => <Customer customers={customers} {...props}/>}/>
    </Switch>
  </div>)
};


class Customers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customers: []
    }
  }

  componentDidMount() {
    const data = [
	  {
		"id": 1,
		"clientname": "ABC Company",
		"type": "Client",
		"parent_client": "",
		"phone": "647-555-5555",
		"email": "cardinalsgolfclub@abc.com",
		"mailing_street": "411 Hanlan Road",
		"mailing_city": "Woodbridge",
		"last_interaction": "20201022",
		"last_action": "Document XXX uploaded and sent to client",
		"status": "Link Accessed",
		"notification_badge": "",
		"updated": "July 30, 2020",
		"received_from": "Robert Client",
		"size": "828 KB",
		"name": "2017AbcDocument.pdf"
		},
		{
		"id": 2,
		"clientname": "Bob's Cable Company",
		"type": "Client",
		"parent_client": "",
		"phone": "647-555-5556",
		"email": "bobscable@abc.com",
		"mailing_street": "15 Milford Ave",
		"mailing_city": "Toronto",
		"last_interaction": "20201019",
		"last_action": "Requested Document XXX from client",
		"status": "Expired",
		"notification_badge": "*",
		"updated": "July 3, 2020",
		"received_from": "Joe Client",
		"size": "8 Files, 4 MB",
		"name": "Cardinal Golf Club XXX Folder A"
		},
		{
		"id": 3,
		"clientname": "Craig's Cookies Ltd.",
		"type": "Client",
		"parent_client": "",
		"phone": "647-555-5554",
		"email": "craigcookies@abc.com",
		"mailing_street": "640 Wonderland Road N",
		"mailing_city": "London",
		"last_interaction": "20200929",
		"last_action": "Received Document XXX from client",
		"status": "Failed",
		"notification_badge": "*",
		"updated": "June 20, 2020",
		"received_from": "Sarah Client",
		"size": "5 Files, 2 MB",
		"name": "20182019LegalDocuments"
		},
		{
		"id": 4,
		"clientname": "Patricia Cook Inc.",
		"type": "Prospect",
		"parent_client": "",
		"phone": "647-555-5550",
		"email": "patriciacook@abc.com",
		"mailing_street": "999 Milden Rd",
		"mailing_city": "Chia",
		"last_interaction": "20201018",
		"last_action": "Document XXX uploaded and sent to client",
		"status": "Pending",
		"notification_badge": "",
		"updated": "July 3, 2020",
		"received_from": "Joe Client",
		"size": "120 KB",
		"name": "XXXDocumentCardinalGolfClub.doc"
		},
		{
		"id": 5,
		"clientname": "Automotive Company Inc.",
		"type": "Client",
		"parent_client": "",
		"phone": "647-555-5540",
		"email": "automotivecorp@abc.com",
		"mailing_street": "363-43rd St W",
		"mailing_city": "Prince Albert",
		"last_interaction": "20201020",
		"last_action": "Requested Document XXX from client",
		"status": "Pending",
		"notification_badge": "",
		"updated": "May 15, 2020",
		"received_from": "Joe Client",
		"size": "400 KB",
		"name": "2017AbcDocument.pdf"
		},
		{
		"id": 6,
		"clientname": "Town of Caledon",
		"type": "Client",
		"parent_client": "",
		"phone": "647-555-5333",
		"email": "caledon@caledon.ca",
		"mailing_street": "6311 Old Church Rd",
		"mailing_city": "Caledon",
		"last_interaction": "20201025",
		"last_action": "Document XXX uploaded and sent to client",
		"status": "Incomplete",
		"notification_badge": "",
		"updated": "May 8, 2020",
		"received_from": "Robert Client",
		"size": "3 Files, 3 MB",
		"name": "Cardinal Golf Club XXX Folder A"
		},
		{
		"id": 7,
		"clientname": "City of Oshawa",
		"type": "Client",
		"parent_client": "",
		"phone": "647-555-2222",
		"email": "oshawa@oshawa.ca",
		"mailing_street": "411 Hanlan Road",
		"mailing_city": "Oshawa",
		"last_interaction": "20201023",
		"last_action": "Requested Document XXX from client",
		"status": "Expired",
		"notification_badge": "*",
		"updated": "May 4, 2020",
		"received_from": "Joe Client",
		"size": "5 Files, 2 MB",
		"name": "20182019LegalDocuments.pdf"
		},
		{
		"id": 8,
		"clientname": "City of Pickering",
		"type": "Client",
		"parent_client": "",
		"phone": "647-555-1111",
		"email": "service@pickering.ca",
		"mailing_street": "One The Esplanade",
		"mailing_city": "Pickering",
		"last_interaction": "20201025",
		"last_action": "Document XXX Upload Incomplete",
		"status": "Failed",
		"notification_badge": "*",
		"updated": "June 1, 2020",
		"received_from": "Sarah Client",
		"size": "120 KB",
		"name": "XXXDocumentCardinalGolfClub.doc"
		},
		{
		"id": 9,
		"clientname": "City of Markham",
		"type": "Client",
		"parent_client": "",
		"phone": "647-555-7777",
		"email": "info@markham.ca",
		"mailing_street": "101 Town Centre Boulevard",
		"mailing_city": "Markham",
		"last_interaction": "20201020",
		"last_action": "Document XXX uploaded and sent to client",
		"status": "Link Accessed",
		"notification_badge": "",
		"updated": "April 29, 2020",
		"received_from": "Robert Client",
		"size": "400 KB",
		"name": "2017AbcDocument.pdf"
		},
		{
		"id": 10,
		"clientname": "City of Whitby",
		"type": "Client",
		"parent_client": "",
		"phone": "647-555-9999",
		"email": "service@whitby.ca",
		"mailing_street": "575 Rossland Road East",
		"mailing_city": "Whitby",
		"last_interaction": "20201025",
		"last_action": "Requested Document XXX from client",
		"status": "Pending",
		"notification_badge": "",
		"updated": "April 25, 2020",
		"received_from": "Joe Client",
		"size": "3 Files 3 MB",
		"name": "Cardinal Golf Club XXX Folder A"
		}
    ];

    this.setState({customers: data})
  }

  render() {
    const {customers} = this.state;

    return (
      <Layout>
        <CustomersDisplay customers={customers} />
      </Layout>
    )
  }
}

export default Customers;
