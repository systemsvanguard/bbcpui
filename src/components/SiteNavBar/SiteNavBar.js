// src/components/SiteNavBar/SiteNavBar.js
import React from 'react';
import { NavLink } from 'react-router-dom';
import NavLogo from '../../images/logo-red-200px.svg';
import './NavBar';

export default function SiteNavBar() {
  return (
    <nav className="navbar is-transparent" role="navigation" aria-label="main navigation">
      <div className="navbar-brand">
        <NavLink to="/" className="navbar-item">
          {/*
            <img src={process.env.PUBLIC_URL + 'images/logo-red-200px.svg'} alt="Site Logo" title="Financial File Upload" />
          */}
          <img src={NavLogo} alt="Site Logo" title="Financial File Upload" />
        </NavLink>

        <button className="navbar-burger burger button is-danger" aria-label="menu" aria-expanded="false" data-target="navMenu">
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </button>
      </div>

      <div id="navMenu" className="navbar-menu">
        <div className="navbar-end">
          <NavLink to="/"          className="navbar-item">Home</NavLink>

          {/*  Take out Client link later; just for testing now  */}
          <NavLink to="/clients"   className="navbar-item"> * Clients * </NavLink>
          <NavLink to="/about"     className="navbar-item">About</NavLink>
          <NavLink to="/customers" className="navbar-item">Customers</NavLink>
          <NavLink to="/products"  className="navbar-item">Products</NavLink>
          <NavLink to="/resources" className="navbar-item">Resources</NavLink>
          <NavLink to="/contact"   className="navbar-item">Contact</NavLink>
        </div>
      </div>
    </nav>
  )
}
