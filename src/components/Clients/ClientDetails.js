// src/components/Clients/ClientDetails.js
import React, { Component } from "react";
import { Link } from "react-router-dom";
// import { render } from "react-dom";
import cardData from '../../supporting/clientData.json'

class ClientDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      results: []
    };
  }

  render() {
    const id = this.props.match.params.id;
    const data = cardData.data.Client.find(item => item.id === id);

    return (
      // Card details compoment
      <div className="card-details">
        <h2><strong>ID: </strong>  {data.id}</h2>
        <h2><strong><em>Client :</em>  {data.clientname}</strong> </h2>
        <h2>Ph: {data.phone} | Email: {data.email} </h2>
        <h2>Address: {data.mailing_street}, Email: {data.mailing_city} </h2>

        <Link
          to={{
            pathname: "/clients",
            state: this.props.location.state
          }}
        >
          <button>Return to List</button>
        </Link>
      </div>
    );
  }
}

export default ClientDetails;

