// src/components/pages/Clients.js
import React  from "react";
// import ReactDOM from "react-dom";
import { Link } from "react-router-dom";
import { render } from "react-dom";
import cardData from '../../supporting/clientData.json'
import ClientDetails from './ClientDetails';

class Clients extends React.Component {
  state = { data: [] };
  componentDidMount() {
    this.setState({ data: cardData.data.Client });
  }

  render() {
    const cardKeys = Object.keys(cardData);
    return (
      <div className="scroll-list">
        {cardData.data.Client.map((results, index) => {
          return (
            <div className="col-sm-3">
              <div className="card our-team" id="employeeInfo">
                <div className="card-body">
                  <p>
                    <strong>Client: {results.clientname}
                    </strong> | {results.id} |
                    <Link
                      to={{ pathname: `/client/${results.id}`, state: results }}
                      className={`card-wrapper restore-${results.index}`}
                    >
                      {results.clientname}
                    </Link>
                  </p>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    );
  }
}

export default Clients;
