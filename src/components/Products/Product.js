// src/components/Products/Product.js
import React from "react";
import { useParams } from "react-router-dom";

const Product = ({ data }) => {
  const { productId } = useParams();
  const product = data.find((p) => p.id === Number(productId));
  let productData;

  if (product) {
    productData = (
      <div>
        <h3><strong>{product.clientname}</strong></h3>
        <p>Address: {product.mailing_street}, {product.mailing_city}</p>
        <p>Ph: {product.phone} | Email: {product.email}</p>
        <p>Type: {product.type} | ID: {product.id}</p>
        <h4><strong><em>Status</em> : {product.status}</strong> </h4>
      </div>
    );
  } else {
    productData = <h2>Oops!. Product does not exist in our DB</h2>;
  }

  return (
    <div style={{ display: "flex" }}>
      <div
        style={{
          width: "80%",
          margin: "auto",
          background: "#ffffff"
        }}
      >
        {productData}
      </div>
    </div>
  );
};

export default Product;
