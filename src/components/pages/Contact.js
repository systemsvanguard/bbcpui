// src/components/pages/Contact.js
import React from 'react';
import Finance03 from '../../images/finance03.jpg';

export default function Contact() {
  return (
    <section className="section">
      <div className="container">

        <img src={Finance03} className="img_responsive" alt="Financal Banking App" title="Financal Banking App" />
        <h1 className="myheadings">Contact</h1>

        <p>Landlubber or just lubber Sail ho fore piracy lugsail topmast coxswain pink. Red ensign bounty gangplank lad crack Jennys tea cup Gold Road ho cog. Line plunder lad Jack Ketch bounty jury mast topmast cog. Lateen sail aft chandler loaded to the gunwalls wherry ho scuttle hulk. Quarterdeck lad overhaul six pounders nipper grog blossom cutlass sutler.</p><br />
        <p>Piracy Buccaneer sutler Jack Tar red ensign long clothes interloper fore. Tackle fire ship rigging ballast scourge of the seven seas ahoy swing the lead lookout. </p>
        <p>Wherry flogging spyglass mizzen loot fore line shrouds. Smartly landlubber or just lubber bilge rat bilge water Jack Ketch black jack weigh anchor yo-ho-ho. Knave cackle fruit gangway bring a spring upon her cable mizzenmast hogshead hearties log.</p><br />
      </div>
    </section>
  )
}
