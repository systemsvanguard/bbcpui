// src/components/pages/About.js
import React from 'react';
import Finance09 from '../../images/finance09.png';

export default function About() {
  return (
    <section className="section">
      <div className="container">

        <img src={Finance09} className="img_responsive" alt="Financal Banking App" title="Financal Banking App" />
        <h1 className="myheadings">About</h1>

        <p>Bilged on her anchor ye nipperkin skysail Shiver me timbers topmast broadside lugger. Chase guns weigh anchor Corsair skysail lookout yawl poop deck black jack. Quarterdeck scuppers broadside aft grapple sutler nipperkin weigh anchor. Clap of thunder poop deck hornswaggle crimp no prey, no pay loot blow the man down handsomely. Nelsons folly knave avast Davy Jones' Locker stern grog blossom walk the plank Corsair.</p><br />
        <p>Take a caulk matey shrouds yawl Blimey hands Pieces of Eight lass. Sheet bring a spring upon her cable cog parley hogshead squiffy carouser jolly boat. Chain Shot gaff wherry bring a spring upon her cable capstan no prey, no pay topmast bounty. Log careen poop deck schooner black jack fore coffer Plate Fleet. American Main holystone Letter of Marque jib case shot flogging maroon overhaul.
        </p>
      </div>
    </section>
  )
}
