// src/components/pages/Home.js
import React from 'react';
import RecentlySearched from '../RecentlySearched/RecentlySearched';
import LatestActivity from '../LatestActivity/LatestActivity';

export default function Home() {
  return (
    <section className="section">
      <div className="container">

        <RecentlySearched /><br />
        <LatestActivity /><br />

      </div>
    </section>
  )
}
