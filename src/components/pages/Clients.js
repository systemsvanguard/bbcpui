// src/components/pages/Clients.js
import React from 'react';
import Finance10 from '../../images/finance10.png';

export default function Clients() {
  return (
    <section className="section">
      <div className="container">

        <img src={Finance10} className="img_responsive" alt="Financal Banking App" title="Financal Banking App" />
        <h1 className="myheadings">Clients</h1>

        <p>Privateer careen man-of-war gangplank fluke nipper provost hearties. Yawl Sink me Jack Ketch black jack shrouds take a caulk reef sails chase. Black spot long boat fire ship yard booty swing the lead lugger chase guns. Killick warp Gold Road Jolly Roger grog blossom Jack Tar scurvy avast. Measured fer yer chains Plate Fleet run a rig marooned spirits hands booty Jack Tar.</p><br />
      </div>
    </section>
  )
}
