// src/components/pages/NotFound.js
import React from 'react';

export default function NotFound() {
  return (
    <section className="section">
      <div className="container">

        <h1 className="myheadings">Oops! Page Not Found</h1>

        <img src={process.env.PUBLIC_URL + 'images/not_found.jpg'  } alt="Resource Not Found" title="Page Not Found" />
        <p>We are very sorry! We cannot find the resource you are looking for.  Please try again?</p><br /><br />
        <p>Killick warp Gold Road Jolly Roger grog blossom Jack Tar scurvy avast. Measured fer yer chains Plate Fleet run a rig marooned spirits hands booty Jack Tar.</p><br />
      </div>
    </section>
  )
}
