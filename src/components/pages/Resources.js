// src/components/pages/Resources.js
import React from 'react';
import Finance13 from '../../images/finance13.jpg';

export default function Resources() {
  return (
    <section className="section">
      <div className="container">

        <img src={Finance13} className="img_responsive" alt="Financal Banking App" title="Financal Banking App" />
        <h1 className="myheadings">Resources</h1>

        <p>Blow the man down brig Jack Ketch league hulk lugsail gun yard. Grog blossom Sink me Shiver me timbers rum skysail ahoy wench black spot.</p>
        <p>Bucko hardtack scurvy Plate Fleet pink black jack tender walk the plank. Ye Yellow Jack yo-ho-ho Cat o'nine tails nipperkin chantey Admiral of the Black landlubber or just lubber. Crack Jennys tea cup coxswain haul wind parrel chase guns jury mast broadside wench.</p><br />
        <p>Barque hail-shot plunder scuttle jib lass heave to red ensign. Chandler hogshead galleon gangway topsail topgallant parrel take a caulk. Holystone killick lugger</p>
        <p>Sail ho Jack Tar port scurvy furl. Aft pirate gaff marooned hail-shot Jack Tar warp quarter. Red ensign sloop Plate Fleet case shot American Main belaying pin provost hail-shot.</p><br />
      </div>
    </section>
  )
}
